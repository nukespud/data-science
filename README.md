# Data Science and Machine Learning Working Meeting

https://gitlab.com/nukespud/data-science

## Day 1 
1)  [Introduction to Data Science](https://docs.google.com/presentation/d/130KE_aB-2IcaVUgWJykZSFqLONGS4LL9tOEEM6NRgYs/edit?usp=sharing)
1) [Git version control and command line](gitlabBasics.md)
1)  [Python Introduction and Numpy](https://colab.research.google.com/drive/1KuZd0q3MPYPY44WDzRpGKNcYTSgU2ius)
1) [Data Processing with Python](https://colab.research.google.com/drive/1M1nLS4nqIsPzU72Mrg37Df4jgmfhbqds)

### Lunch (On your own)
1) [Sampling](https://colab.research.google.com/drive/1AVGUTWlGHu2XN7xWCr1onfZvSaq9zE5t)
1) [Python Pandas GroupBy](https://colab.research.google.com/drive/1MqF-O-oXY2xiFaXWwc8jUjNf-qgSqSFw)
1) [Python Pandas,Functions, & Histograms](https://colab.research.google.com/drive/16Lz67ooPZdjwUnjQTDntbg4vzfOoxLRf)
1) Simple Machine Learning
    1) [Statistics and Regression](https://colab.research.google.com/drive/10ofi8qzolJIOWLvJ5pjdVXgZRVbM4Ewr)
    2) [Linear Regression](https://colab.research.google.com/drive/1FkIlc2aHDAXwL0X71VMIYrHWqjOhL6V7)

## Day 2

https://gitlab.com/nukespud/data-science

1) [Introduction to Machine Learning](https://docs.google.com/presentation/d/147kSgdaI07_DkhEFiwJhno7gj_uKG8SJouPIYlEG4NI/edit?usp=sharing)
2) [Data Visualization with Bokeh](https://colab.research.google.com/drive/11Cg0w8oGzSmYOm4B63jI2W8p4xTySuWX)
3) [Predicting with Decision Trees](https://colab.research.google.com/drive/1Dc0RKjHN0cbcW6mtCEJ9ncFiOtFGgUlS#scrollTo=XPeT2dOdOJsU)
4) [Natural Language Processing with NLTK](https://colab.research.google.com/drive/18TN4sXYVLDDhdZ8ttdMRupUs62XjFuRO)

### Lunch (On your own)

1) Case Study: Credit Card Defaults
    1) [Cleaning and Feature Engineering](https://colab.research.google.com/drive/1uO12S-myWW_o0xPNV9KmTffHJ35HPoOp)
    2) [Credit Card Defaults with Logistic Regression, Random Forest, and Naive Bayes](https://colab.research.google.com/drive/1JjjUd7yKwSWm2jPrqnYQvSsxliWLx7ze)
1) [Clustering with k-Menas](https://colab.research.google.com/drive/1qi8OjUFFj-ZY0Ysk3dKPY47d4bjwYtoM)
1) [Brainstorm Funding](https://docs.google.com/document/d/1sxGJxM7A7WvUjjTQBffSrBi-LhDN5bSfEpUtNUakQp8/edit?usp=sharing)

1) [MNIST Digits and AutoML notebooks](https://gitlab.com/CEADS/DrKerby/python/)
    1) [MNIST with traditional ML](https://colab.research.google.com/drive/18sZvSPRGZ6VFD2R_SpndaA0W5LI-BEtC)
    1) [MNIST with TPOT](https://colab.research.google.com/drive/1RR4zFSFpjSfCuOb8BXobvP8KjsNfYxP7)
    1) [MNIST with Autokeras](https://colab.research.google.com/drive/1KaRg0r4RnLcT4mbyrZkgR3nxPos9KzBl)



## Booster sessions

* Go to the bottom of the website https://gitlab.com/nukespud/data-science 

[Day 4.  HDF5 and SQL with Python](https://colab.research.google.com/drive/17eS8NFtKqQQ-D4e2QSU4Sl0TWT6YNtnP)