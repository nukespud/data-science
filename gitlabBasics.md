* Note: The majority of this tutorial comes from Gitlab https://gitlab.com/gitlab-org/gitlab-ce/tree/master/doc/gitlab-basics

# GitLab basics

Step-by-step guides on the basics of working with Git and GitLab.
## Working directly with gitlab.com
1. Create a gitlab account on gitlab.com at 
    * https://gitlab.com/users/sign_in and 
    * conform your email 
2. Sign in to gitlab https://gitlab.com/#login-pane
2. [Create a project](create-project.md)
    * edit the readme file
    * commit changes
    * look at comments
2. Create a branch
    * Edit readme file again
    * Switch branches
    * Look at Graph
    * compare new branch with master
2. [Create a merge request](add-merge-request.md) 
    * Edit information
    * Submitt request   
    * Merge
    * Add comment
    * Resolve thread
    * Delete source branch
    * Look at the new branches
    * Look at Graph
2. Look at how to create a group
2. [Fork a project](fork-project.md)
2. Look at settings/repository


##  Working with gitlab from command line
* **NOTE** If you don't have git on your computer you can use one online at https://rootnroll.com/d/fish-shell/

2. [Start using Git on the command line](start-using-git.md)
3. [Additional Notes](gitLab.md)
